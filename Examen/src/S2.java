import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class S2 extends JFrame {
    JLabel op1Label, op2Label;
    JTextField textFieldOp1, textFieldOp2, textFieldResult;
    JButton buttonSave;

    S2() {
        setTitle("Calculator");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(190, 180);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;

        op1Label = new JLabel("Operand 1");
        op1Label.setBounds(10, 20, width, height);

        op2Label = new JLabel("Operand 2");
        op2Label.setBounds(10, 40, width, height);


        textFieldOp1 = new JTextField();
        textFieldOp1.setBounds(80, 20, width, height);

        textFieldOp2 = new JTextField();
        textFieldOp2.setBounds(80, 40, width, height);

        textFieldResult = new JTextField();
        textFieldResult.setBounds(10, 100, 150, height);
        textFieldResult.setEnabled(false);

        buttonSave = new JButton("Calculeaza");
        buttonSave.setBounds(10, 80, 150, height);

        buttonSave.addActionListener(new S2.SaveActions());

        add(op1Label);
        add(op2Label);
        add(textFieldOp1);
        add(textFieldOp2);
        add(textFieldResult);
        add(buttonSave);
    }

    class SaveActions implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int op1 = Integer.parseInt(S2.this.textFieldOp1.getText());
            int op2 = Integer.parseInt(S2.this.textFieldOp2.getText());
            int rez = op1 + op2;

            S2.this.textFieldResult.setText(String.valueOf(rez));
        }
    }

    public static void main(String[] args) {
        new S2();
    }
}