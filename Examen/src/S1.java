public class S1 {

}

class A {
    private M m;
}

class B {

}

class K extends L {

}

class L {
    private M m;
    private int a;

    public L() {
    }

    public void b() {
    }

    public void i() {
    }

    public void setM(M m) {
        this.m = m;
    }
}

class M {
    B b;

    public M() {
        b = new B();
    }
}

class X {
    public void met(L i) {

    }
}
